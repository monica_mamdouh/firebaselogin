package com.example.monicamamdouh.loginfirebasenew.common.helpers;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.google.firebase.auth.FirebaseAuth;


/**
 * Created by Monica.Mamdouh on 8/3/2017.
 */

public class MyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());


    }

    public static FirebaseAuth getAuthUser() {

        return FirebaseAuth.getInstance();

    }




}
