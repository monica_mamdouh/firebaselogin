package com.example.monicamamdouh.loginfirebasenew.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.example.monicamamdouh.loginfirebasenew.R;
import com.example.monicamamdouh.loginfirebasenew.common.base.BaseActivity;

public class ActivityLogin extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        initializeViews();
        setListeners();


        loadFragmentLogin();

    }

    @Override
    protected void initializeViews() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        fragment.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void setListeners() {

    }

    private void loadFragmentLogin() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, FragmentLogin.newInstance()).commit();
    }

    public static void startActivityLogin(Context context) {
        Intent i = new Intent(context, ActivityLogin.class);
        context.startActivity(i);
        ((Activity) context).finish();


    }


}
