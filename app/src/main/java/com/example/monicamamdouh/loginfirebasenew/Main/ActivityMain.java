package com.example.monicamamdouh.loginfirebasenew.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.example.monicamamdouh.loginfirebasenew.R;
import com.example.monicamamdouh.loginfirebasenew.common.base.BaseActivity;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.Constants;
import com.example.monicamamdouh.loginfirebasenew.common.models.UserFireBase;

public class ActivityMain extends BaseActivity {

    private UserFireBase mUserFireBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUserFireBase = getIntent().getParcelableExtra(Constants.USER_FIREBASE_EXTRA);
        loadFragmentMain();
    }

    public static void startActivityMain(Context context, UserFireBase userFireBase) {
        Intent i = new Intent(context, ActivityMain.class);
        i.putExtra(Constants.USER_FIREBASE_EXTRA, userFireBase);
        context.startActivity(i);
        ((Activity) context).finish();

    }

    @Override
    protected void initializeViews() {

    }

    @Override
    protected void setListeners() {

    }

    private void loadFragmentMain() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, FragmentMain.newInstance(mUserFireBase)).commit();
    }
}
