package com.example.monicamamdouh.loginfirebasenew.Main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.monicamamdouh.loginfirebasenew.R;
import com.example.monicamamdouh.loginfirebasenew.common.base.BaseFragment;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.AppPreferences;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.Constants;
import com.example.monicamamdouh.loginfirebasenew.common.models.UserFireBase;
import com.example.monicamamdouh.loginfirebasenew.login.ActivityLogin;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMain extends BaseFragment implements MainView {

    private UserFireBase mFireBaseUser;
    private MainPresenter mainPresenter;

    private Button btnLogOut;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mFireBaseUser = getArguments().getParcelable(Constants.USER_FIREBASE_EXTRA);
        }
    }


    public FragmentMain() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        initializeViews(v);
        setListeners();
        mainPresenter = new MainPresenter(getActivity(), this);

        return v;

    }

    public static FragmentMain newInstance(UserFireBase user) {
        FragmentMain fragmentMain = new FragmentMain();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER_FIREBASE_EXTRA, user);
        fragmentMain.setArguments(bundle);
        return fragmentMain;
    }

    @Override
    protected void initializeViews(View v) {
        ImageView imgUserPic = (ImageView) v.findViewById(R.id.imgUserPic);
        TextView txtUserName = (TextView) v.findViewById(R.id.txtUserName);

        if (mFireBaseUser != null) {
            Picasso.with(getActivity()).load(mFireBaseUser.getUserPic()).into(imgUserPic);
            txtUserName.setText(mFireBaseUser.getUserName());
        }

        btnLogOut = (Button) v.findViewById(R.id.btnLogOut);


    }

    private View.OnClickListener btnLogOutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            mainPresenter.LogOut();

        }
    };

    @Override
    protected void setListeners() {
        btnLogOut.setOnClickListener(btnLogOutClickListener);

    }

    @Override
    public void onUserLogOut() {
        AppPreferences.setString(Constants.NAME, null, getContext());
        AppPreferences.setString(Constants.PHOTO, null, getContext());
        ActivityLogin.startActivityLogin(getActivity());
    }
}
