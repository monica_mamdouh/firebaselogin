package com.example.monicamamdouh.loginfirebasenew.login;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.monicamamdouh.loginfirebasenew.common.base.BasePresenter;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.Constants;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.MyApplication;
import com.example.monicamamdouh.loginfirebasenew.common.models.UserFireBase;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.util.Arrays;

/**
 * Created by Monica.Mamdouh on 10/26/2017.
 */

public class LoginPresenter extends BasePresenter {

    private FirebaseAuth mAuth;
    private Context context;
    private AccessTokenTracker mAccessTokenTracker;
    private LoginManager loginManager;
    private FirebaseUser user = null;
    private LoginView loginView;

    private CallbackManager facebookCallbackManager;
    private static final String TAG = LoginPresenter.class.getSimpleName();

    LoginPresenter(Context context, LoginView loginView) {
        this.context = context;
        this.loginView = loginView;

    }

    CallbackManager fbLoginUsingFireBase() {


        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            }
        };

        loginManager = LoginManager.getInstance();
        facebookCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
        return facebookCallbackManager;

    }


    void handleFacebookLogin(Activity activity) {

        mAccessTokenTracker.startTracking();
        loginManager.logInWithReadPermissions(activity, Arrays.asList(Constants.PUBLIC_PROFILE));


    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth = MyApplication.getAuthUser();

        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");

                    user = mAuth.getCurrentUser();
                    if (user != null) {

                        UserFireBase userFireBase = new UserFireBase(user.getDisplayName(), user.getPhotoUrl().toString());

                        loginView.onLoginFacebook(userFireBase);
                    }
                } else {
                    Log.d(TAG, "signInWithCredential:fail");

                }

            }
        });


    }

    TwitterAuthClient handleTwitterLogin(Activity activity) {
        //twitter
        mAuth = MyApplication.getAuthUser();

        TwitterConfig config = new TwitterConfig.Builder(context)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_KEY_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        TwitterAuthClient mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                handleTwitterSession(result.data);

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        return mTwitterAuthClient;
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);


        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {

                                UserFireBase userFireBase = new UserFireBase(user.getDisplayName(), user.getPhotoUrl().toString());

                                loginView.onLoginTwitter(userFireBase);
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                        }

                    }
                });
    }

    GoogleApiClient setUpGooglePlus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("498660668435-p47d0p6rb87lv2rrliitn8fl1eiivta0.apps.googleusercontent.com")
                .requestEmail()
                .build();


        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        return mGoogleApiClient;
    }

    void firebaseAuthWithGoogle(GoogleSignInAccount acct, Activity activity) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        mAuth = MyApplication.getAuthUser();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null) {
                        UserFireBase userFireBase = new UserFireBase(user.getDisplayName(), user.getPhotoUrl().toString());


                        loginView.onLoginGoogle(userFireBase);
                    }

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.getException());

                }

            }
        });
    }


}
