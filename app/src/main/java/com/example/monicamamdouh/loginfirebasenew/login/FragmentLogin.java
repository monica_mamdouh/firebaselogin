package com.example.monicamamdouh.loginfirebasenew.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.monicamamdouh.loginfirebasenew.Main.ActivityMain;
import com.example.monicamamdouh.loginfirebasenew.R;
import com.example.monicamamdouh.loginfirebasenew.common.base.BaseFragment;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.AppPreferences;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.Constants;
import com.example.monicamamdouh.loginfirebasenew.common.models.UserFireBase;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLogin extends BaseFragment implements LoginView {

    private Button btnFacebook;
    private Button btnTwitter;
    private TwitterAuthClient mTwitterAuthClient;
    private ProgressBar progressLogin;


    private CallbackManager facebookCallbackManager;
    private LoginPresenter loginPresenter;
    private Button btnGoogle;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;


    public FragmentLogin() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String userName = AppPreferences.getString(Constants.NAME, getActivity(), null);
        if (userName != null) {
            String userImage = AppPreferences.getString(Constants.PHOTO, getActivity(), null);
            UserFireBase userFireBase = new UserFireBase(userName, userImage);
            ActivityMain.startActivityMain(getActivity(), userFireBase);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initializeViews(view);
        setListeners();
        loginPresenter = new LoginPresenter(getActivity(), this);


        return view;
    }

    @Override
    protected void initializeViews(View v) {
        btnFacebook = (Button) v.findViewById(R.id.btnFacebookLogin);
        btnTwitter = (Button) v.findViewById(R.id.btnTwitterLogin);
        btnGoogle = (Button) v.findViewById(R.id.btnGooglePlus);
        progressLogin = (ProgressBar) v.findViewById(R.id.progress_login);


    }

    @Override
    protected void setListeners() {
        btnFacebook.setOnClickListener(fbLoginClickListener);
        btnTwitter.setOnClickListener(twitterLoginClickListener);
        btnGoogle.setOnClickListener(googleLoginClickListener);

    }

    private View.OnClickListener googleLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mGoogleApiClient = loginPresenter.setUpGooglePlus();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    };
    private View.OnClickListener twitterLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mTwitterAuthClient = loginPresenter.handleTwitterLogin(getActivity());
        }
    };

    private View.OnClickListener fbLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            facebookCallbackManager = loginPresenter.fbLoginUsingFireBase();
            loginPresenter.handleFacebookLogin(getActivity());

        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        showProgress(true);


        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = result.getSignInAccount();

                    loginPresenter.firebaseAuthWithGoogle(account, getActivity());

                }
            }
        } else if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }


    public static FragmentLogin newInstance() {
        return new FragmentLogin();
    }

    @Override
    public void onLoginFacebook(UserFireBase userFireBase) {
        setLoginResult(userFireBase);

    }

    private void setLoginResult(UserFireBase userFireBase) {
        showProgress(false);
        AppPreferences.setString(Constants.NAME, userFireBase.getUserName(), getContext());
        AppPreferences.setString(Constants.PHOTO, userFireBase.getUserPic(), getContext());
        ActivityMain.startActivityMain(getContext(), userFireBase);
    }

    @Override
    public void onLoginTwitter(UserFireBase userFireBase) {
        setLoginResult(userFireBase);

    }

    @Override
    public void onLoginGoogle(UserFireBase userFireBase) {
        setLoginResult(userFireBase);

    }

    public void showProgress(boolean show) {
        if (show) {
            progressLogin.setVisibility(View.VISIBLE);

        } else {
            progressLogin.setVisibility(View.GONE);

        }
    }
}
