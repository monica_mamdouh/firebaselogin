package com.example.monicamamdouh.loginfirebasenew.Main;

import android.content.Context;

import com.example.monicamamdouh.loginfirebasenew.common.base.BasePresenter;
import com.example.monicamamdouh.loginfirebasenew.common.helpers.MyApplication;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Monica.Mamdouh on 10/26/2017.
 */

 class MainPresenter extends BasePresenter {

    private MainView mainView;
    private Context context;

    MainPresenter(Context context, MainView mainView) {
        this.context = context;
        this.mainView = mainView;

    }

     void LogOut() {
         FirebaseAuth firebaseAuth = MyApplication.getAuthUser();
        if (firebaseAuth != null) {
            firebaseAuth.signOut();
            mainView.onUserLogOut();

        }

    }

}
