package com.example.monicamamdouh.loginfirebasenew.common.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Monica.Mamdouh on 10/26/2017.
 */

public class UserFireBase implements Parcelable {

    private String userName;
    private String userPic;

    public UserFireBase(String userName, String userPic) {
        this.userName = userName;
        this.userPic = userPic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    protected UserFireBase(Parcel in) {
        userName = in.readString();
        userPic = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(userPic);
    }

    @SuppressWarnings("unused")
    public static final Creator<UserFireBase> CREATOR = new Creator<UserFireBase>() {
        @Override
        public UserFireBase createFromParcel(Parcel in) {
            return new UserFireBase(in);
        }

        @Override
        public UserFireBase[] newArray(int size) {
            return new UserFireBase[size];
        }
    };
}