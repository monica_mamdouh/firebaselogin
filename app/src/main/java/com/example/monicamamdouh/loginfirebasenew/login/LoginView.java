package com.example.monicamamdouh.loginfirebasenew.login;

import com.example.monicamamdouh.loginfirebasenew.common.base.BaseView;
import com.example.monicamamdouh.loginfirebasenew.common.models.UserFireBase;

/**
 * Created by Monica.Mamdouh on 10/26/2017.
 */

public interface LoginView extends BaseView {

    void onLoginFacebook(UserFireBase userFireBase);

    void onLoginTwitter(UserFireBase userFireBase);

    void onLoginGoogle(UserFireBase userFireBase);

}
