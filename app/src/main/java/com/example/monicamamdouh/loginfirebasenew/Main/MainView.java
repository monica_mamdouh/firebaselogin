package com.example.monicamamdouh.loginfirebasenew.Main;

import com.example.monicamamdouh.loginfirebasenew.common.base.BaseView;

/**
 * Created by Monica.Mamdouh on 10/26/2017.
 */

public interface MainView extends BaseView {
    void onUserLogOut();

}
